
print("Welcome to the Basketball Quiz Game!")

#Chances
chances = 1
print("You will have", chances, "chance to answer correctly. \nPlease put the alphabet of the answer\n")


#Score
score = 0

#question number 1
question_1 = print("1) What NBA player scored 100 point in one game ?\n(a) Kareem Abdul-Jabbar \n(b) Bill Russel\n(c) Elgin Baylor\n(d) Wilt Chamberlain\n\n ")
answer_1 = "d"

for i in range(chances):
    answer = input("Answer: ")
    if (answer.lower() == answer_1):
        print("Correct! Good Job.\n")
        score = score + 1
        break
    else:
        print("Incorrect!\n")
        print("The correct answer is", answer_1, "\n\n")



#question number 2
question_2 = print("2) What team owns the longest winning streak in NBA history?\n(a) Los Angeles-Lakers\n(b) Boston-Celtics\n(c) Chicago-Bulls\n(d) Golden State-Wariors\n\n ")
answer_2 = "a"

for i in range(chances):
    answer = input("Answer: ")
    if (answer.lower() == answer_2):
        print("Correct! Good Job.\n")
        score = score + 1
        break
    else:
        print("Incorrect!\n")

        print("The correct answer is", answer_2, "\n\n")



#question number 3
question_3 = print("3) Who is the all-time leading scorer in men's college basketball?\n(a) Pete Maverich\n(b) Stephen Curry\n(c) Michel Jordan\n(d) Freman Williams\n\n ")
answer_3 = "a"

for i in range(chances):
    answer = input("Answer: ")
    if (answer.lower() == answer_3):
        print("Correct! Good Job.\n")
        score = score + 1
        break
    else:
        print("Incorrect!\n")

        print("The correct answer is", answer_3, "\n\n")


#question number 4
question_4 = print("4) Who is the youngest player in NBA history to recorde three triple doubles?\n(a) Ja Morant \n(b) Luka Dončić\n(c) Lebrone James \n(d) Magic Johnson\n\n ")
answer_4 = "b"

for i in range(chances):
    answer = input("Answer: ")
    if (answer.lower() == answer_4):
        print("Correct! Good Job.\n")
        score = score + 1
        break
    else:
        print("Incorrect!\n")

        print("The correct answer is", answer_4, "\n\n")



#question number 5
question_5 = print("5) Whic player holds the NBA record for the most assist in a single game ?\n(a) Scott Skilles\n(b) Rajon Rondo\n(c) Jason Kid\n(d) Steve Nash\n\n ")
answer_5 = "a"

for i in range(chances):
    answer = input("Answer: ")
    if (answer.lower() == answer_5):
        print("Correct! Good Job.\n")
        score = score + 1
        break
    else:
        print("Incorrect!\n")

        print("The correct answer is", answer_5, "\n\n")

# question number 6
question_6= print("6) Who are the Splash Brothers  ?\n(a) S.Curry - K.Thompson\n(b) M.Jordan - S.Pippen\n(c) K.Durent - R.Westbrook\n(d) L.James - K.Irving\n\n ")
answer_6 = "a"

for i in range(chances):
    answer = input("Answer: ")
    if (answer.lower() == answer_6):
        print("Correct! Good Job.\n")
        score = score + 1
        break
    else:
        print("Incorrect!\n")

        print("The correct answer is", answer_6, "\n\n")


#print the score
while score >= 5:
    print("Well done! Your score was", score)
    break

while score <= 4:
    print("Better luck next time! Your score was", score)
    break
#Goodbye message
print("Thank you for playing the Basketball Quiz Game!")
print(" ")